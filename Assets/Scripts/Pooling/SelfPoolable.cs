﻿using Pooling.Poolers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfPoolable : MonoBehaviour
{
    public Poolable poolable;
    public SetPooler pool;

    public void Enqueue()
    {
        pool.Enqueue(poolable);
    }
}


public static class SelfPoolableGameobjectExtension
{
    public static void ClearSelfPoolables(this GameObject go)
    {
        SelfPoolable[] poolables = go.GetComponentsInChildren<SelfPoolable>();
        foreach (SelfPoolable item in poolables)
        {
            item.Enqueue();
        }
    }
}