﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Pooling.Poolers
{
	public class SetPooler : BasePooler 
	{
		#region Fields / Properties
		[ShowInInspector,ReadOnly]
		public HashSet<Poolable> collection = new HashSet<Poolable>();
		#endregion

		#region Public

		public SetPooler SetPrefab(GameObject nPrefab)
		{
			prefab = nPrefab;
			return this;
		}

		[Button]
		public override void Enqueue (Poolable item)
		{
			base.Enqueue(item);
			if (collection.Contains(item))
				collection.Remove(item);
			/*else
            Debug.LogWarning("Doesnt contains item");*/
		}

		[Button]
		public override Poolable Dequeue ()
		{
			Poolable item = base.Dequeue();
			collection.Add(item);
			return item;
		}

		[Button]
		public override void EnqueueAll ()
		{
			foreach (Poolable item in collection)
				base.Enqueue(item);
			collection.Clear();
		}

		#endregion
	}
}
