﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class BattleFieldViewTester : MonoBehaviour
{
    public BattleFieldView battleFieldView;

    private BattleField battleField;

    public Vector2Int size = new Vector2Int(3, 3);

    private void Start()
    {
        Initialize();
    }

    public void Initialize()
    {
        battleField = new BattleField(size);
        battleFieldView.Initialize(battleField);
    }

    [Button]
    public void Highlight(int boardId, List<Vector2Int> selection)
    {
        List<TileView> tileViews = GetTileViewsFromCoordinate(boardId, selection);
        foreach (TileView tView in tileViews)
            tView.transform.localPosition = tView.transform.localPosition + Vector3.up;
    }

    private List<TileView> GetTileViewsFromCoordinate(int boardId, List<Vector2Int> selection)
    {
        List<Tile> tiles = battleField.GetTiles(boardId, selection);
        List<TileView> tileViews = battleFieldView.GetTileViews(tiles);
        return tileViews;
    }

    [Button]
    public void HighlightFrom(int boardId, Vector2Int center, List<Vector2Int> displacedTilesPositions)
    {
        List<Tile> tiles = battleField.GetDisplacedTilesFrom(boardId, center, displacedTilesPositions);
        List<TileView> tileViews = battleFieldView.GetTileViews(tiles);
        foreach (TileView tView in tileViews)
            tView.transform.localPosition = tView.transform.localPosition + Vector3.up;
    }

    [Button]
    public void Clear()
    {
        Debug.Log("Clear");
        battleFieldView.ResetBoardPositions();
    }
}
