﻿using Pooling.Poolers;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardView : MonoBehaviour
{
    public Board Value { get; private set; }
    public  Vector2 RealSize { get; private set; }
    public SetPooler tileViewPool;

    private TileView[,] tileViews;

    public float tileSize = 1;
    public float tileSeparation = 0;

    public BoardView Initialize(Board board, Transform parent)
    {
        Value = board;
        RealSize = new Vector2(board.Size.x * tileSize + (board.Size.x - 1) * tileSeparation, board.Size.y * tileSize + (board.Size.y - 1) * tileSeparation);
        CreateTileViews(board.tiles);
        ResetTilePositions();
        transform.SetParent(parent);
        return this;
    }

    public TileView this[Vector2Int coordinate] => tileViews[coordinate.x, coordinate.y];

    public void ResetTilePositions()
    {
        Vector3 startPosition = (tileViews.GetLength(0) / 2 * Vector3.left) + (tileViews.GetLength(1) / 2 * Vector3.forward);
        for (int x = 0; x < tileViews.GetLength(0); x++)
        {
            for (int y = 0; y < tileViews.GetLength(1); y++)
            {
                tileViews[x, y].transform.localPosition = startPosition + (x * Vector3.right + y * Vector3.forward);
                tileViews[x, y].transform.localScale = Vector3.one * tileSize;
            }
        }
    }


    private void CreateTileViews(Tile[,] tiles)
    {
        tileViews = new TileView[tiles.GetLength(0), tiles.GetLength(1)];
        for (int x = 0; x < tiles.GetLength(0); x++)
        {
            for (int y = 0; y < tiles.GetLength(1); y++)
            {
                TileView currentTileView = tileViewPool.Dequeue().GetComponent<TileView>();
                tileViews[x,y] = currentTileView.Initialize(tiles[x, y],transform);
            }
        }
    }
}
