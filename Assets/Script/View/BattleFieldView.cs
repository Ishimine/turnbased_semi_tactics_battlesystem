﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Pooling.Poolers;
using System;

public class BattleFieldView : MonoBehaviour
{
    public BattleField Value { get; private set; }

    public float boardsDistance = 3;

    public SetPooler boardPool;

    private BoardView[] boardViews;

    public BattleFieldView Initialize(BattleField battleField)
    {
        Value = battleField;
        CreateViews();
        return this;
    }

    private void CreateViews()
    {
        boardViews = new BoardView[2];
        for (int i = 0; i < Value.Boards.Length; i++)
        {
            Poolable go = boardPool.Dequeue();
            Debug.Log($"Is Null {go == null}");
            boardViews[i] = go.GetComponent<BoardView>();
            boardViews[i].Initialize(Value.Boards[i],transform);
        }
        ResetBoardPositions();
    }

    internal void ResetBoardPositions()
    {
        for (int i = 0; i < Value.Boards.Length; i++)
        {
            int dir = ((i == 0) ? -1 : 1);
            boardViews[i].transform.localPosition = dir * boardsDistance / 2 * Vector3.right + dir * boardViews[i].RealSize.x * Vector3.right / 2;
            boardViews[i].ResetTilePositions();
        }
    }

    internal List<TileView> GetTileViews(List<Tile> tiles)
    {
        List<TileView> retValue = new List<TileView>();
        foreach (Tile cTile in tiles)
            retValue.Add(GetTileView(cTile));
        return retValue;
    }

    private TileView GetTileView(Tile cTile)
    {
        return boardViews[cTile.Board.Index][cTile.Coordinate];
    }
}
