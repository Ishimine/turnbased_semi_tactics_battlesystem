﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileView : MonoBehaviour
{
    public Tile Value { get; private set; }
    public TileView Initialize(Tile tile, Transform parent)
    {
        transform.SetParent(parent);
        Value = tile;
        gameObject.name = $"Tile: [{Value.X},{Value.Y}]";
        return this;
    }
}
