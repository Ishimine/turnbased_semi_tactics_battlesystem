﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleField
{
    public Board[] Boards { get; private set; }

    public Board PlayerBoard => Boards[0];
    public Board EnemyBoard => Boards[1];

    public BattleField(Vector2Int boardSize)
    {
        Boards = new Board[2] { new Board(0, boardSize) , new Board(1, boardSize) };
    }

    internal List<Tile> GetTiles(int boardId, List<Vector2Int> selection)
    {
        List<Tile> tiles = new List<Tile>();
        for (int i = 0; i < selection.Count; i++)
            tiles.Add(GetTile(boardId, selection[i]));
        return tiles;
    }

    public Tile GetTile(int boardId, Vector2Int coordinate)
    {
        return Boards[boardId][coordinate];
    }

    internal List<Tile> GetDisplacedTilesFrom(int boardId, Vector2Int center, List<Vector2Int> displacedTilesPositions)
    {
        return Boards[boardId].GetDisplacedTilesFrom(center, displacedTilesPositions);
    }
}
