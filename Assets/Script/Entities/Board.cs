﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board 
{
    public int Index { get; private set; }
    public readonly Vector2Int Size;
    public Tile[,] tiles;

    public Tile this[Vector2Int coordinate]
    {
        get => tiles[coordinate.x, coordinate.y];
    }

    public Board(int boardIndex, Vector2Int size)
    {
        Index = boardIndex;
        this.Size = size;
        CreateTiles(size);
    }

    private void CreateTiles(Vector2Int size)
    {
        tiles = new Tile[size.x, size.y];
        for (int x = 0; x < size.x; x++)
            for (int y = 0; y < size.y; y++)
                tiles[x, y] = new Tile(this, new Vector2Int(x,y));
    }

    internal List<Tile> GetDisplacedTilesFrom(Vector2Int center, List<Vector2Int> displacedTilesPositions)
    {
        List<Tile> rTiles = new List<Tile>();
        for (int i = 0; i < displacedTilesPositions.Count; i++)
        {
            Vector2Int endCoordinate = center + displacedTilesPositions[i];
            if (IsValid(endCoordinate))
                rTiles.Add(this[endCoordinate]);
        }
        return rTiles;
    }

    private bool IsValid(Vector2Int endCoordinate)
    {
        return (endCoordinate.x >= 0 && endCoordinate.x < Size.x) && (endCoordinate.y >= 0 && endCoordinate.y < Size.y);
    }
}
