﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile
{
    public Board Board { get; private set; }
    public object Content { get; private set; } = null;

    public readonly Vector2Int Coordinate;

    public bool IsEmpty => Content == null;

    public int X => Coordinate.x;
    public int Y => Coordinate.x;

    public Tile (Board board, Vector2Int coordinate)
    {
        Board = board;
        this.Coordinate = coordinate;
    }

    public void SetContent(object nContent)
    {
        Content = nContent;
    }

}
